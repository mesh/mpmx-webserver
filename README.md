# MPMX Web

Web service for running MPMX simulations on a server.

## Running

A development/production environment can be provisioned using [Vagrant][].

```sh
vagrant up
```

[Vagrant]: https://www.vagrantup.com/
