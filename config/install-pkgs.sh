#!/usr/bin/env bash
set -o errexit
set -o nounset
set -o pipefail
shopt -s nullglob globstar

readonly aur_deps=(
	"caddy2"
	"seexpr"
	"partio"
	# "mpmx"
	# "python-mpmxweb"
)

# Import GPG key to validate Caddy2 source files
gpg \
	--keyserver pool.sks-keyservers.net \
	--receive-keys 29D0817A67156E4F25DC24782A349DD577D586A5

for pkg_name in "${aur_deps[@]}"; do
	git clone --depth 1 "https://aur.archlinux.org/""$pkg_name"".git"

	pushd "$pkg_name"

	# Patch SeExpr for LLVM 10 compatibility
	if [[ "$pkg_name" = "seexpr" ]]; then
		git apply /vagrant/config/seexpr-llvm10.patch
	fi

	# Build package
	MAKEFLAGS="-j""$(nproc)" makepkg --noconfirm --noprogressbar -rc

	# Install built packages as root
	sudo pacman --noconfirm --noprogressbar -U *.zst

	popd  # $pkg_name

	# Clean-up package repo
	rm -rf "$pkg_name"
done
