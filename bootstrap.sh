#!/usr/bin/env bash
set -o errexit
set -o nounset
set -o pipefail
shopt -s nullglob globstar

readonly deps=(
	"base-devel"
	"boost"
	"cmake"
	"cuda"
	"doxygen"
	"freeglut"
	"gcc"
	"git"
	"glu"
	"go"
	"gtest"
	"llvm10"
	"llvm10-libs"
	"nvidia"
	"postgresql"
	"python"
	"python-flask"
	"python-pip"
	"python-pyqt5"
	"python-sip"
	"qt5-base"
	"sip"
	"swig"
	"uwsgi"
)

pac() {
	pacman --noconfirm --noprogressbar "$@"
}

# Upgrade keyring, we may fail to sign some keys otherwise
pac -Sy "archlinux-keyring"
pac -Su

# Install mpmx/mpmxweb dependencies
pac --needed -Sy "${deps[@]}"

# While testing the Vagrant provisioning in may be prudent to install CUDA
# manually from a pre-downloaded offline file. CUDA is by far the largest
# package out of all the dependencies (2GB+) so downloading the package through
# pacman every time we test the box is not practical.
#
# In order to install from an offline file:
#
# 1. Select "Download From Mirror" from the following link:
#
# https://www.archlinux.org/packages/community/x86_64/cuda/
#
# 2. Move the downloaded file to the same directory as the Vagrantfile
# 3. Comment out "cuda" from the list of dependencies
# 4. Uncomment the following line (the version no. may need to be adjusted):
# pac -U "/vagrant/cuda-11.1.1-1-x86_64.pkg.tar.zst"

# Downgrade dep versions for SeExpr 3.0.1-1
pac -U "https://archive.archlinux.org/packages/s/sip/sip-4.19.22-2-x86_64.pkg.tar.zst"
pac -U "https://archive.archlinux.org/packages/p/python-sip/python-sip-4.19.22-2-x86_64.pkg.tar.zst"

# TODO(rycwo): Add sip and python-sip to IgnorePkg list

# Install "non-standard" packages from the Arch Linux User Repository (AUR).
# Note that building the packages is done as the "vagrant" user since running
# makepkg as root is forbidden.
sudo -iu vagrant bash /vagrant/config/install-pkgs.sh

# Configure and start PostgreSQL
sudo -iu postgres \
	initdb --locale en_US.UTF-8 -E UTF8 -D /var/lib/postgres/data
systemctl enable --now postgresql.service

# Configure and start uWSGI
cp /vagrant/config/mpmxweb.ini /etc/uwsgi
systemctl enable --now uwsgi@mpmxweb

# Configure and start Caddy (reverse proxy)
cp /vagrant/config/Caddyfile /etc/caddy
systemctl enable --now caddy

cat << EOF
Provisioning was successful!

SSH into the virtual machine to start developing:

	vagrant ssh

If you are developing mpmxweb and would like to run Flask's debug server instead
of uWSGI, first disable the systemd service:

	sudo systemctl stop uwsgi@mpmxweb

Then run the debug server:

	cd /vagrant
	python run.py
EOF

exit 0
