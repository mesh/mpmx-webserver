"""Web service for running MPMX simulations on a server.

Todo:
    - Database (PostgreSQL, SQLAlchemy)
    - OAuth2
    - Logging/monitoring (Sentry/Rollbar)
"""
from flask import Flask, abort, request


app = Flask(__name__)
# Currently limit file uploads to 1024MB (1GB)
app.config["MAX_CONTENT_LENGTH"] = 1024 * 1024 * 1024


def upload_file(f):
    # TODO(rycwo): Store somewhere on the server and write file path to db
    pass


@app.route("/")
def root():
    return "Hello world!"


@app.route("/api/job/<job_id>")
def job_GET(job_id):
    return {}


@app.route("/api/job", methods=["POST"])
def job_POST():
    return {}


@app.route("/api/job/<job_id>/<file_id>", methods=["POST"])
def job_file_POST(job_id, file_id):
    if "file" not in request.files:
        abort(400)

    # TODO(rycwo): request.remote_addr can be used to default allow upload IPs

    f = request.files["file"]

    # Determine file type from mimetype or extension
    # f.mimetype
    # f.filename

    upload_file(f)

    # TODO(rycwo): Give some response
    return {}


@app.errorhandler(404)
def not_found(error):
    return "404"
