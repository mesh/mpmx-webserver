if __name__ == "__main__":
    from mpmxweb import app

    app.run(host="0.0.0.0", port="3030", debug=True)
